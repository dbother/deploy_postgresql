## 功能

* 安装单机PG
* 安装高可用PG（基于patroni）
* 需要有VIP


## 限制

* 基于CentOS7 打包
* 安装包版本为 PG12
* 安装其他版本需自行编译安装下 pgbackrest
* 安装其他版本需自行准备安装包
* 默认安装了watchdog防止极端情况下的脑裂问题
  * 由于 watchdog会重启服务器，因此开启wacthdog的话需要PG是独占服务器，不需要watchdog的可选择注释掉安装部分中的watchdog安装信息



## 示例环境

* 环境：CentOS7
* PG-12
* 使用yum安装



```shell
# ./pg_install.sh 
Usage: ./pg_install.sh [OPTIONS]
  single                 安装单机版PG
  ha                     安装HA的PG，使用patroni组件
  etcd                   安装ETCD，ha版的
  clearall               清除部署PG相关的所有中间件与环境信息
  clear_etcd             清除 etcd 相关环境
  clear_pg               清除 PG 相关环境
```

## 安装单机

修改配置文件 config.cnf 后

```shell
./pg_install.sh single
```

## 安装高可用

1. 配置时间同步（略 ntpdate 或其他）

2. 配置hosts，将三台机器的hostname与IP配置进去（建议）

3. 修改配置文件 config.cnf

4. 安装 ETCD

   ```shell
   # 三台机器分别执行
   ./pg_install.sh etcd
   
   # 查看
   etcdctl member list
   
   # api是v2版本的话
   etcdctl cluster-health
   ```

5. 安装PG

   ```shell
   # 计划作为主节点的
   ./pg_install.sh ha
   
   # 等待第一台安装完成后，后面两台都执行
   ./pg_install.sh ha
   ```

6. 检查

   ```shell
   # 任意一台执行，结果输出如下，同步模式的话Role有一个为sync
   [root@test155 pginstall]# patronictl -c /etc/patroni/patroni.yml list
   + Cluster: pgsql ---------+--------------+---------+----+-----------+
   | Member | Host           | Role         | State   | TL | Lag in MB |
   +--------+----------------+--------------+---------+----+-----------+
   | pg155  | 192.168.60.155 | Leader       | running | 11 |           |
   | pg156  | 192.168.60.156 | Replica 	  | running | 11 |         0 |
   | pg157  | 192.168.60.157 | Replica      | running | 11 |         0 |
   +--------+----------------+--------------+---------+----+-----------+
   
   # 检查 VIP 是否在主库上
   
   # 测试连接
   ```

   

## 安装其他版本PG

* 需要可联网或自行制作对应版本yum源

* pgbackrest 请自行打包替换为对应PG版本的

* 修改脚本参数 pg_install.sh

  ```shell
  # 需要修改pgbackrest_name为对应的打包上传的名称
  pgbackrest_name=pgbackrest12
  # yum 对应安装的版本路径
  pg_home=/usr/pgsql-12
  ```

## 故障动作
| 故障位置 | 故障场景                           | patroni的动作                                                | 数据同步   | 修正处理                                                     |      |
| -------- | ---------------------------------- | ------------------------------------------------------------ | ---------- | ------------------------------------------------------------ | ---- |
| 备库     | kill PG 或 停止PG                  | 自动拉起备库                                                 | 正常       | 无需处理                                                     |      |
| 备库     | 停止patroni                        | 同时停止备库PG                                               | 修正后正常 | 启动patroni                                                  |      |
| 备库     | kill patroni                       | 无操作，PG仍然存活正常复制                                   | 修正后正常 | 启动patroni                                                  |      |
| 主库     | kill PG 或 停止PG                  | 自动拉起主库，不会进行漂移切换操作                           | 正常       |                                                              |      |
| 主库     | 停止patroni                        | 同时停止库PG，并触发failover                                 | 正常       | 启动patroni                                                  |      |
| 主库     | kill patroni                       | 无操作，PG仍然存活正常复制                                   |            |                                                              |      |
| 机房停电 | 所有patroni和PG ，ETCD全部停止     |                                                              | 可能异常   | ETCD会自启动，但为了防止数据丢失，需要自行上去将原主先启动起来。 数据会以第一个启动的服务器数据未准，可能会出现丢数据的情况。 |      |
| 主库     | 主库服务器与其他服务器之间网络中断 | 自动将主库降级为从库                                         | 正常       |                                                              |      |
| 主库     | 关闭网卡，无wacthdog               | 发生切换，其他服务器提升为主继续提供服务重启网卡后，原主被提升为主，并以原主修正数据数据丢失 | 异常       |                                                              |      |
| 主库     | 关闭网卡，有wacthdog               | 主服务器重启重启后启动patroni会作为从加入集群                | 正常       | 重启patroni                                                  |      |